﻿
//Процедура отправки сообщений в PostLink
//Параметры:
// - Сообщения - массив структур вида (Получатели - массив структур вида (Получатель, ЭтоКонференция), Сообщение, Параметры (Структура параметров), Вложения)
// - ХостСервераКоманд
// - ПортTCP - порт на сервере, открытый для приема сообщений
// - Лог - лог сообщений

Процедура ОтправитьСообщенияВPostLink(Сообщения, ХостСервераКоманд = "srvcluster2.stc-spb.ru", ПортTCP = 3457, Ошибки = Неопределено, ОтправленоПакетов = 0) Экспорт
	
	Если ТипЗнч(Сообщения) <> Тип("Массив") Или
		Сообщения.Количество() = 0 Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Ошибки = Новый Массив();
	
	Попытка
		
			Соединение = Новый COMОбъект("MSWinsock.Winsock");
	Исключение
		
		Ошибки.Добавить(ОписаниеОшибки());
		Возврат;
		
	КонецПопытки;
	
	Если ПодключитьсяКСерверу(ХостСервераКоманд, ПортTCP, Ошибки) Тогда
		
		ШаблонСтруктурыСообщения = СформироватьШаблонСтруктурыСообщения();
		
		Для Каждого Сообщение Из Сообщения Цикл
			
			Для Каждого Получатель Из Сообщение.Получатели Цикл
								
				ПакетДляОтправки = СформироватьПакетДляОтправки(Сообщение.ТекстСообщения, Сообщение.Параметры, Получатель, Сообщение.Вложения, ШаблонСтруктурыСообщения);
				Попытка
					
					Соединение.SendData(ПакетДляОтправки);
					ОтправленоПакетов = ОтправленоПакетов + 1;
					Пауза(1);
					
				Исключение
					
					Ошибки.Добавить("Не удалось отправить пакет получателю с ID " + Формат(Получатель.Получатель, "ЧГ=") + "." + Символы.ПС + ОписаниеОшибки())
					
				КонецПопытки;
				
			КонецЦикла;
			
		КонецЦикла;		
		
	КонецЕсли;
	
	
КонецПроцедуры

Функция ПодключитьсяКСерверу(ХостСервераКоманд, ПортTCP, Ошибки);
	
	Соединение.RemoteHost = СокрЛП(ХостСервераКоманд);
	Соединение.RemotePort = ПортTCP;
	КоличествоПауз = 0;
	//Лог = Лог + "Установление соединения с " + Соединение.RemoteHost + ":" + Формат(Соединение.RemotePort, "ЧГ=") + "..." + Символы.ПС;
	
	Попытка
				
		Соединение.Connect();
		
		Пока Соединение.State <> 7 И КоличествоПауз <> 10 Цикл
			Пауза(1);
			КоличествоПауз = КоличествоПауз + 1;
		КонецЦикла;
		
		Если Соединение.State = 7 Тогда
		
			//Лог = Лог + "Соединение установлено." + Символы.ПС;
			Возврат Истина;
			
		Иначе
			Ошибки.Добавить("Не удалось подключиться к серверу команд");
			//Лог = Лог + "Соединение не установлено." + Символы.ПС;
			Возврат Ложь;
			
		КонецЕсли;
		
	Исключение
		Ошибки.Добавить(ОписаниеОшибки());
		Возврат Ложь;
		//Лог = Лог + ОписаниеОшибки() + Символы.ПС;
		//Лог = Лог + "Соединение не установлено." + Символы.ПС;
		Возврат Ложь;
		
	КонецПопытки;
	
КонецФункции

Процедура Пауза(КоличествоСекунд)
	    
    Команда =  "ping -n 1 -w "+Формат(1000 * КоличествоСекунд, "ЧДЦ=0; ЧГ=") + " 127.255.255.255"; 
    ЗапуститьПриложение(Команда,,Истина);
    
КонецПроцедуры

Функция СформироватьШаблонСтруктурыСообщения()
	
	Результат = Новый Структура;
	Результат.Вставить("message");
	Результат.Вставить("receiverId");
	Результат.Вставить("command");
	Результат.Вставить("receiverType");
	
	Возврат Результат;
	
КонецФункции

Функция СформироватьПакетДляОтправки(ТекстСообщения, ПараметрыСообщения, Получатель, Вложения, Знач СтруктураСообщения);
	
	Поток = Новый ПотокВПамяти;
	ЗаписьДанных = Новый ЗаписьДанных(Поток, КодировкаТекста.UTF8);
	ЗаписьДанных.ЗаписатьСимволы(ТекстСообщения);
	Данные = Поток.ЗакрытьИПолучитьДвоичныеДанные();
	
	Поток = Новый ПотокВПамяти;
	ЧтениеДанных = Новый ЧтениеДанных(Данные, КодировкаТекста.Системная);
	ТекстСообщенияUTF = ЧтениеДанных.ПрочитатьСимволы();
	Поток.Закрыть();
	
	СтруктураСообщения.Вставить("message", ?(ПараметрыСообщения.ВФорматеHTML, ТекстСообщенияUTF, СтрЗаменить(ТекстСообщенияUTF, Символы.ПС, "<br/>")));
	СтруктураСообщения.Вставить("receiverId", Получатель.Получатель);
	СтруктураСообщения.Вставить("command", "sendMessage");
	СтруктураСообщения.Вставить("receiverType", ?(Получатель.ЭтоКонференция, "conference", "person"));
		
	ЗаписьJSON = Новый ЗаписьJSON;
	ЗаписьJSON.УстановитьСтроку();
	ЗаписатьJSON(ЗаписьJSON, СтруктураСообщения);
	
	СтрокаСообщения = ЗаписьJSON.Закрыть();
	
	ДлинаСтрокиСообщения = СтрДлина(СтрокаСообщения);
	ДлинаВложения = 0;
	
	//длина информационного пакета строкой
	ПредставлениеДлиныСтрокиСообщения = ПолучитьПредставлениеДлиныСтроки(ДлинаСтрокиСообщения, 4);
	
	Поток = Новый ПотокВПамяти;
	ЗаписьДанных = Новый ЗаписьДанных(Поток, КодировкаТекста.UTF8, ПорядокБайтов.BigEndian);
	
	Счетчик = 0;
	
	Пока Счетчик < 4 Цикл
		
		ЗаписьДанных.ЗаписатьБайт(ЧислоИзШестнадцатеричнойСтроки("0x" + Сред(ПредставлениеДлиныСтрокиСообщения, Счетчик * 2 + 1, 2)));
		Счетчик = Счетчик + 1;
		
	КонецЦикла;
	
	Данные = Поток.ЗакрытьИПолучитьДвоичныеДанные();
	
	Поток = Новый ПотокВПамяти;
	ЧтениеДанных = Новый ЧтениеДанных(Данные, КодировкаТекста.Системная, ПорядокБайтов.BigEndian);
	ДлинаПакетаСтрокой = ЧтениеДанных.ПрочитатьСимволы();
	Поток.Закрыть();
	
	//длина вложений строкой
	ПредставлениеДлиныВложения = ПолучитьПредставлениеДлиныСтроки(ДлинаВложения, 8);
	
	Поток = Новый ПотокВПамяти;
	ЗаписьДанных = Новый ЗаписьДанных(Поток, КодировкаТекста.UTF8, ПорядокБайтов.BigEndian);
	
	Счетчик = 0;
	
	Пока Счетчик < 8 Цикл
		
		ЗаписьДанных.ЗаписатьБайт(ЧислоИзШестнадцатеричнойСтроки("0x" + Сред(ПредставлениеДлиныВложения, Счетчик * 2 + 1, 2)));
		Счетчик = Счетчик + 1;
		
	КонецЦикла;
	
	Данные = Поток.ЗакрытьИПолучитьДвоичныеДанные();
	
	Поток = Новый ПотокВПамяти;
	ЧтениеДанных = Новый ЧтениеДанных(Данные, КодировкаТекста.Системная, ПорядокБайтов.BigEndian);
	ДлинаВложенияСтрокой = ЧтениеДанных.ПрочитатьСимволы();
	Поток.Закрыть();
		
	Возврат ДлинаПакетаСтрокой + ДлинаВложенияСтрокой + СтрокаСообщения;
	
КонецФункции

Функция DecToHex(Знач Число)
	
	Результат = "";
	СтрокаHex = "0123456789ABCDEF";
	
	Пока Число <> 0 Цикл
		
		Поз = Число % 16;
      	Результат = Сред(СтрокаHex, Поз + 1, 1) + Результат;
      	Число = Цел(Число / 16);
		
	КонецЦикла;
      
	Возврат Результат;
	
КонецФункции

Функция ПолучитьПредставлениеДлиныСтроки(ДлинаСтроки, КоличествоБайтов)
	
	Возврат Прав(Формат(0, "ЧЦ=" + Формат(КоличествоБайтов * 2, "ЧГ=") + "; ЧН=; ЧВН=; ЧГ=") + DecToHex(ДлинаСтроки), КоличествоБайтов * 2);
	
КонецФункции

Функция СтрокаИзДвоичныхДанных(Данные)
	
	ДанныеСтрокой = ПолучитьСтрокуИзДвоичныхДанных(Данные, КодировкаТекста.UTF8);
	
	
КонецФункции