﻿
&НаКлиенте
Процедура Добавить(Команда)
	
	МассивСтрокДляУдаления = СписокНоменклатуры.НайтиСтроки(Новый Структура("Пометка",Ложь));
	Для Каждого Строка из МассивСтрокДляУдаления Цикл
		СписокНоменклатуры.Удалить(Строка);
	КонецЦикла;
	
	Оповестить("ДобавитьПомеченнуюНоменклатуру",СписокНоменклатуры,ВладелецФормы);
	ЭтаФорма.Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Для Каждого Строка из Параметры.ТЗНоменклатуры Цикл
		НоваяСтрокаТЧ = СписокНоменклатуры.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрокаТЧ,Строка);
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНоменклатурыПометкаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.СписокНоменклатуры.ТекущиеДанные;
	Если НЕ ТекущиеДанные.Узел Тогда
		Возврат;
	КонецЕсли;
	
	Если ТекущиеДанные.Пометка И ТекущиеДанные.УстановитьВКачествеОсновной И ЗначениеЗаполнено(ТекущиеДанные.Подразделение) Тогда
		Если ТекущиеДанные.СпецификацияУзлаНайдена Тогда
			Оповещение = Новый ОписаниеОповещения("ПослеЗакрытияВопроса",ЭтаФорма, ТекущиеДанные);	
			ПоказатьВопрос(Оповещение,	"Установить данную спецификацию основной для подразделения?",
							РежимДиалогаВопрос.ДаНет,0,	КодВозвратаДиалога.Нет,	"Установить основной для подразделения");
		Иначе
			ПредупредитьСпецификацияУзлаНеЗадана(ТекущиеДанные);
		КонецЕсли;	
	КонецЕсли;	
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗакрытияВопроса(Результат,Параметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		
		СтруктураДанных = Новый Структура("Номенклатура,Спецификация,Подразделение",
				Параметры.Номенклатура,	Параметры.СпецификацияУзла, Параметры.Подразделение);
														
		ДобавитьЗаписьВРегистОсновныеСпецификации(СтруктураДанных);
		
	Иначе
		
		Оповещение = Новый ОписаниеОповещения("ВопросУстановитьСпецификациюОсновнойДляВсех",ЭтаФорма, Параметры);	
		
		ПоказатьВопрос(Оповещение,	"Установить данную спецификацию основной для всех?",
		РежимДиалогаВопрос.ДаНет,0,	КодВозвратаДиалога.Нет,	"Установить основной для всех");
		
	КонецЕсли;	
	
	Параметры.УстановитьВКачествеОсновной = Ложь;
	
КонецПроцедуры	

&НаКлиенте
Процедура ВопросУстановитьСпецификациюОсновнойДляВсех(Результат,Параметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		
		СтруктураДанных = Новый Структура("Номенклатура,Спецификация",
					Параметры.Номенклатура, Параметры.СпецификацияУзла);
					
		ДобавитьЗаписьВРегистОсновныеСпецификации(СтруктураДанных);
	Иначе
		Сообщить("Необходимо выбрать основную спецификацию");
	КонецЕсли;	
	
КонецПроцедуры	

&НаКлиенте
Процедура ОтправитьОповещениеРуководителюПодразделения(Параметры) Экспорт
	
КонецПроцедуры	

&НаКлиенте
Процедура ПредупредитьСпецификацияУзлаНеЗадана(Параметры) 
	
	    Оповещение = Новый ОписаниеОповещения("ОтправитьОповещениеРуководителюПодразделения", ЭтаФорма, Параметры);	
		ТекстОповещения = "Необходимо выбрать спецификацию для узла. Руководителю подразделения будет отправлено оповещение об обработке спецификации.";
		ПоказатьПредупреждение(Оповещение, ТекстОповещения,	10,	"Спецификацию для узла не выбрана");
								
КонецПроцедуры	

&НаСервере
Процедура ДобавитьЗаписьВРегистОсновныеСпецификации(СтруктураДанных)
	
	МенеджерЗаписи = РегистрыСведений.СТЦ_IGR_ОсновныеСпецификации.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи,СтруктураДанных);
	МенеджерЗаписи.Записать();
	
КонецПроцедуры
