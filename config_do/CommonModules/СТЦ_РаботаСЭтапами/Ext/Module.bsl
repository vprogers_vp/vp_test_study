﻿
// Заполняет коды СДР всех этапов проекта 
Процедура ЗаполнитьКодыСДРЭтапов(Проект) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	// Коды СДР обычных этапов
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Этапы.Ссылка,
	|	Этапы.НомерЭтапаВУровне,
	|	Этапы.ПометкаУдаления,
	|	Этапы.КодСДР
	|ИЗ
	|	Справочник.СТЦ_Этапы КАК Этапы
	|ГДЕ
	|	Этапы.Владелец = &Проект
	|	И Этапы.Родитель = ЗНАЧЕНИЕ(Справочник.СТЦ_Этапы.ПустаяСсылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Этапы.НомерЭтапаВУровне,
	|	Этапы.Наименование";
	Запрос.УстановитьПараметр("Проект", Проект);
	
	Результат = Запрос.Выполнить();	
	Если Результат.Пустой() Тогда
		Возврат;
	КонецЕсли;
	
	Счетчик = 1;
	Выборка = Результат.Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Если Выборка.ПометкаУдаления Тогда
			КодСДР = "";
			НомерЭтапаВУровне = 9999;
		Иначе
			КодСДР = Строка(Счетчик);
			НомерЭтапаВУровне = Счетчик;
			
			Счетчик = Счетчик + 1;
		КонецЕсли;
		
		Если (Выборка.НомерЭтапаВУровне <> НомерЭтапаВУровне) Или (Выборка.КодСДР <> КодСДР) Тогда 
			//ЗаблокироватьДанныеДляРедактирования(Выборка.Ссылка);
			ЭтапОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
			ЭтапОбъект.НомерЭтапаВУровне = НомерЭтапаВУровне;
			ЭтапОбъект.КодСДР = КодСДР;
			
			ЭтапОбъект.Записать();
		КонецЕсли; 
		
		ЗаполнитьКодыСДРПодчиненныхЭтапов(Выборка.Ссылка);
	КонецЦикла;
	
	ЗаполнитьПорядокЭтапов(Проект);
	
КонецПроцедуры

// Заполняет коды СДР подчиненных этапов 
Процедура ЗаполнитьКодыСДРПодчиненныхЭтапов(Родитель, ИзмененныеЭтапы = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если ИзмененныеЭтапы = Неопределено Тогда 
		ИзмененныеЭтапы = Новый Массив;
	КонецЕсли;	
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Этапы.Ссылка,
	|	Этапы.НомерЭтапаВУровне,
	|	Этапы.ПометкаУдаления,
	|	Этапы.КодСДР
	|ИЗ
	|	Справочник.СТЦ_Этапы КАК Этапы
	|ГДЕ
	|	Этапы.Родитель = &Родитель
	|
	|УПОРЯДОЧИТЬ ПО
	|	Этапы.НомерЭтапаВУровне,
	|	Этапы.Наименование";
	
	Запрос.УстановитьПараметр("Родитель", Родитель);
	
	Результат = Запрос.Выполнить();	
	Если Результат.Пустой() Тогда
		Возврат;
	КонецЕсли;
	
	КодРодителя = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Родитель, "КодСДР");
	
	Счетчик = 1;
	Выборка = Результат.Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Если Выборка.ПометкаУдаления Тогда
			КодСДР = "";
			НомерЭтапаВУровне = 9999;
		Иначе	
			Если Не ЗначениеЗаполнено(КодРодителя) Тогда
				КодСДР = Строка(Счетчик);
			Иначе
				КодСДР = КодРодителя + "." + Строка(Счетчик);
			КонецЕсли;
			НомерЭтапаВУровне = Счетчик;
			
			Счетчик = Счетчик + 1;
		КонецЕсли;
		
		Если (Выборка.НомерЭтапаВУровне <> НомерЭтапаВУровне) Или (Выборка.КодСДР <> КодСДР) Тогда 
		 	//ЗаблокироватьДанныеДляРедактирования(Выборка.Ссылка);
			ЭтапОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
			ЭтапОбъект.НомерЭтапаВУровне = НомерЭтапаВУровне;
			ЭтапОбъект.КодСДР = КодСДР; 
		
			ЭтапОбъект.Записать();
			ИзмененныеЭтапы.Добавить(ЭтапОбъект.Ссылка);
		КонецЕсли;
		
		ЗаполнитьКодыСДРПодчиненныхЭтапов(Выборка.Ссылка, ИзмененныеЭтапы);
	КонецЦикла;	
	
КонецПроцедуры

// Возвращает максимальный номер этапа в пределах родителя 
Функция ПолучитьМаксимальныйНомерЭтапаУровня(Проект, Родитель) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ЕСТЬNULL(МАКСИМУМ(Этапы.НомерЭтапаВУровне), 0) КАК НомерЭтапаВУровне
	|ИЗ
	|	Справочник.СТЦ_Этапы КАК Этапы
	|ГДЕ
	|	Этапы.Владелец = &Проект
	|	И Этапы.Родитель = &Родитель
	|	И НЕ Этапы.ПометкаУдаления";
		
	Запрос.УстановитьПараметр("Родитель", Родитель);	
	Запрос.УстановитьПараметр("Проект", Проект);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Возврат Выборка.НомерЭтапаВУровне;
	КонецЕсли;
	
	Возврат 0;
	
КонецФункции

// Возвращает массив этапов одного уровня с переданным 
Функция ПолучитьМассивЭтаповОдногоУровняСУказанным(Проект, Этап) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Этапы.Ссылка
	|ИЗ
	|	Справочник.СТЦ_Этапы КАК Этапы
	|ГДЕ
	|	Этапы.Владелец = &Проект
	|	И Этапы.Родитель = &Родитель
	|	И НЕ Этапы.ПометкаУдаления";
	
	Родитель = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Этап, "Родитель");
	Запрос.УстановитьПараметр("Родитель", Родитель);
	Запрос.УстановитьПараметр("Проект", Проект);	
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
	
КонецФункции

// Возвращает Код СДР и номер этапа
Функция ПолучитьКодСДРИНомерЭтапаВУровне(Проект, Родитель) Экспорт 
	
	МаксимальныйНомер = СТЦ_РаботаСЭтапами.ПолучитьМаксимальныйНомерЭтапаУровня(Проект, Родитель);
	
	КодСДРСтрока = "";
	Если ЗначениеЗаполнено(Родитель) Тогда
		КодСДРСтрока = Родитель.КодСДР;	
	КонецЕсли;
	
	НомерЭтапаВУровне = МаксимальныйНомер + 1;
	КодСДР = КодСДРСтрока
		+ ?(ЗначениеЗаполнено(КодСДРСтрока), ".", "")
		+ Строка(НомерЭтапаВУровне);
		
	ДанныеВозврата = Новый Структура;
	ДанныеВозврата.Вставить("КодСДР", КодСДР);
	ДанныеВозврата.Вставить("НомерЭтапаВУровне", НомерЭтапаВУровне);
	
	Возврат ДанныеВозврата;
	
КонецФункции


// Заполняет порядок этапов в общей структуре проекта
//
// Параметры:
//  Проект - СправочникСсылка.Проект - ссылка на проект, для этапов которого необходимо заполнить порядок.
//
Процедура ЗаполнитьПорядокЭтапов(Проект) Экспорт 
	
	УстановитьПривилегированныйРежим(Истина);
	
	НомерЭтапа = 1;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Этапы.Ссылка
	|ИЗ
	|	Справочник.СТЦ_Этапы КАК Этапы
	|ГДЕ
	|	Этапы.Владелец = &Проект
	|	И Этапы.Родитель = ЗНАЧЕНИЕ(Справочник.СТЦ_Этапы.ПустаяСсылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Этапы.НомерЭтапаВУровне,
	|	Этапы.Наименование";
		
	Запрос.УстановитьПараметр("Проект", Проект);
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл 
		РегистрыСведений.СТЦ_ПорядокЭтапов.УстановитьПорядокЭтапа(
			Выборка.Ссылка, НомерЭтапа);
		НомерЭтапа = НомерЭтапа + 1;
		ЗаполнитьПорядокПодчиненныхЭтапов(Выборка.Ссылка, НомерЭтапа)
	КонецЦикла;
	
КонецПроцедуры

// Заполняет порядок подчиненных этапов 
Процедура ЗаполнитьПорядокПодчиненныхЭтапов(Родитель, НомерЭтапа)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Этапы.Ссылка
	|ИЗ
	|	Справочник.СТЦ_Этапы КАК Этапы
	|ГДЕ
	|	Этапы.Родитель = &Родитель
	|
	|УПОРЯДОЧИТЬ ПО
	|	Этапы.НомерЭтапаВУровне,
	|	Этапы.Наименование";
	
	Запрос.УстановитьПараметр("Родитель", Родитель);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		РегистрыСведений.СТЦ_ПорядокЭтапов.УстановитьПорядокЭтапа(
			Выборка.Ссылка, НомерЭтапа);
		НомерЭтапа = НомерЭтапа + 1;
		ЗаполнитьПорядокПодчиненныхЭтапов(Выборка.Ссылка, НомерЭтапа)
	КонецЦикла;
	
КонецПроцедуры

